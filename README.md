# Project LysNS
## Description
LysNS is a dynamic DNS project created in order to bring a dynamic
domain name solution to the members for Lysator.

This in hopes to
further help Lysators members in their endeavors experimenting with
servers at home.

## LysNS Overview Diagram
The following image is a initial overview diagram of the LysNS
solution.

![solution-overview](./documentation/dia/solution-overview/solution-overview.jpeg)

### More Information To Come
<!-- https://guides.github.com/features/mastering-markdown/#examples
link to site with markdown language guide -->

## Future ideas

### user.lysns.lysator.se domain

Provide a public domain which users can register subdomains to.
git.lysator.liu.se/lysns/lysadm-flask branch user-domain contains a proof of
concept where a domain with owner uid = 0 means that it's shared.
The problem with this is that subdomains don't have an owner field, but inherits
that information from their domain. Which means that security is currently bust
when multiple people share one domain.

It could also be possible to provide the same thing by simple telling users that
<uname>.user.lysns.lysator.se is free to register, and that we have the propper
NS records to handle it.


