#!/usr/bin/env python3
import argparse
import os
import socket
import ssl

import ip_query_pb2

MAX_MSG_LEN = 512

def request_address(address, port, cert):
    """
    This function obtains the external ip address of the current machine.
    """
    msg = ip_query_pb2.Query()

    connection = ssl.wrap_socket(socket.create_connection((address, port,)),
                                 cert_reqs=ssl.CERT_REQUIRED, ca_certs=cert)

    buf = connection.recv(MAX_MSG_LEN)

    answer = ip_query_pb2.Query()
    answer.ParseFromString(buf)

    return answer.address

def nsupdate(domain_server, domain, ip):
    res = os.system('''nsupdate -k key.key -p 2223 <<EOF
                       server {}
                       update delete {}
                       update add {} 600 A {}
                       send'''.format(domain_server, domain, domain, ip))
    if res != 0:
        print('Got an error when updating the domain!')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='LysNS IP update client.')
    parser.add_argument('server', type=str, help='Server address.')
    parser.add_argument('port', type=int, help='Server port.')
    parser.add_argument('cert', type=str, help='Public certificate for the server.')
    parser.add_argument('domain_server', type=str, help='The address of the DNS server you wish to update.')
    parser.add_argument('domain', type=str, help='The domain record you wish to update.')
    parser.add_argument('--ipv6', action='store_true', help='Perform an IPv6 update.')
    args = parser.parse_args()

    address = request_address(args.server, args.port, args.cert)

    print('Got {} from the server.'.format(address))

    nsupdate(args.domain_server, args.domain, address)
