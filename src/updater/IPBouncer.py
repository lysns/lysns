#!/usr/bin/env python3
import socket
import socketserver
import argparse
import ssl

import ip_query_pb2

class RequestHandler(socketserver.BaseRequestHandler):
    def handle(self):
        resp = ip_query_pb2.Query()
        resp.address = self.client_address[0]

        self.request.sendall(resp.SerializeToString())

class ForkingServer(socketserver.ForkingMixIn, socketserver.TCPServer):
    def __init__(self, address, handler, family):
        self.address_family = family
        super().__init__(address, handler)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='LysNS IP update server.')
    parser.add_argument('host', type=str, help='The listening address of the server.')
    parser.add_argument('port', type=int, help='The listening port of the server.')
    parser.add_argument('cert', type=str, help='The SSL certificate of the server.')
    parser.add_argument('key', type=str, help='The private key for the SSL certificate.')
    parser.add_argument('--ipv6', action='store_true', help='The given listening address is IPv6.')
    args = parser.parse_args()

    family = socket.AF_INET

    if args.ipv6:
        family = socket.AF_INET6

    server = ForkingServer((args.host, args.port), RequestHandler, family)

    server.socket = ssl.wrap_socket(server.socket,
                                    server_side = True,
                                    certfile = args.cert,
                                    keyfile = args.key)

    server.serve_forever()
