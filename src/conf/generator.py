#!/usr/bin/env python3
from jinja2 import FileSystemLoader, Environment
from db import DB
from socket import gethostbyname
from dns.resolver import Resolver
from dnssec_keygen import dnssec_keygen
import os

# Represents the client key for updating a DNS record.
class Key:
    def __init__(self, name, algorithm, secret):
        self.name = name
        self.algorithm = algorithm
        self.secret = secret

# Represents a zone configuration in named.conf.
class Zone:
    def __init__(self, name, keys, zonefile):
        self.name = name
        self.keys = keys
        self.zonefile = zonefile

# Represents an update policy. Key is allowed to update domain.
class ZoneKey:
    def __init__(self, key, domain):
        self.key = key
        self.domain = domain

# Represents a master zone file.
class ZoneFile:
    def __init__(self, domain, primary_nameserver,
                 nameserver, hostmaster, recordlist):
        self.domain = domain
        self.primary_nameserver = primary_nameserver
        self.nameserver = nameserver
        self.hostmaster = hostmaster
        self.recordlist = recordlist

# Represents DNS records to put in the master zone file.
class Record:
    def __init__(self, subdomain, ttl, type, address):
        self.subdomain = subdomain
        self.ttl = ttl
        self.type = type
        self.address = address

# Resolve fully qualified domain name
def lookup(fully_qualified_domain_name):
    resolver = Resolver()
    resolver.nameservers=[gethostbyname('127.0.0.1')]
    ips = list()
    try:
        ips += [str(x) for x in resolver.query(fully_qualified_domain_name, 'A')]
    except:
        pass
    try:
        ips += [str(x) for x in resolver.query(fully_qualified_domain_name, 'AAAA')]
    except:
        pass
    return ips

def application(env, start_response):
    env = Environment(loader = FileSystemLoader('.'))

    # Generate keys
    with DB() as d:
        for id in d.get_domain_ids():
            domain_name = d.get_domain_name(id)
            for sid in d.get_subdomain_ids_without_keys(id):
                fqdn = d.get_fqdn(domain_name, sid)
                with dnssec_keygen() as dk:
                    public_key, private_key = dk.create_keys(fqdn)
                    if not d.set_subdomain_keys(sid, public_key, private_key):
                        print('Could not save keys for {}!'.format(sid))

    # Keys
    # XXX This file should be included from named.conf
    with open("/usr/local/etc/namedb/keys", "w") as keys_file, DB() as d:
        for id in d.get_domain_ids():
            domain_name = d.get_domain_name(id)
            for sid in d.get_subdomain_ids(id):
                key_template = env.get_template('key.template')
                fqdn = d.get_fqdn(domain_name, sid)
                key = Key(fqdn, 'hmac-md5', d.get_key(sid))
                rendered_key = key_template.render(keylist = [ key ])
                keys_file.write(rendered_key)

    # Zones
    zones = list()
    with DB() as d:
        for id in d.get_domain_ids():
            domain_name = d.get_domain_name(id)
            zone_template = env.get_template('zone.template')
            zone_keys = list()
            for sid in d.get_subdomain_ids(id):
                fqdn = d.get_fqdn(domain_name, sid)
                key = Key(fqdn, 'hmac-md5', d.get_key(sid))
                zone_key = ZoneKey(key.name, fqdn)
                zone_keys.append(zone_key)
            zone = Zone(domain_name,
                        zone_keys,
                        '/usr/local/etc/namedb/master/' + domain_name)
            zones.append(zone)
    rendered_zone = zone_template.render(zonelist = zones)

    # XXX This file should be included from named.conf
    with open("/usr/local/etc/namedb/zones", "w") as zones_file:
        zones_file.write(rendered_zone)

    # Zonefile
    with DB() as d:
        for id in d.get_domain_ids():
            domain_name = d.get_domain_name(id)
            zonefile_template = env.get_template('zonefile.template')
            records = list()
            for sid in d.get_subdomain_ids(id):
                rr = d.get_record(sid)
                fqdn = d.get_fqdn(domain_name, sid)
                for ip in lookup(fqdn):
                    record = Record(rr.get('name'),
                                    rr.get('ttl'),
                                    rr.get('record_types.name'),
                                    ip)
                    records.append(record)
            zonefile = ZoneFile(domain_name,
                                'lysns.lysator.liu.se',
                                'lysns.lysator.liu.se',
                                'hostmaster.' + domain_name,
                                records)
            rendered_zonefile = zonefile_template.render(domain = zonefile.domain,
                                    primary_nameserver = zonefile.primary_nameserver,
                                    nameserver = zonefile.nameserver,
                                    hostmaster = zonefile.hostmaster,
                                    recordlist = zonefile.recordlist)
            with open("/usr/local/etc/namedb/master/" + domain_name, "w") as zonefile_file:
                zonefile_file.write(rendered_zonefile)

    # Reload bind config to import new keys
    if os.system('/usr/local/sbin/rndc reload') != 0:
        print('Could not restart named!')

    start_response('200 OK', [('Content-Type', 'text/html')])
    return [b""]
