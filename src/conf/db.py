import pymysql

class DB():
    def __init__(self):
        with open("/usr/local/etc/lysns/db-pass", "r") as f:
            self.pw = f.readline().strip()

    def __enter__(self):
        self.connection = pymysql.connect(host='lysns.lysator.liu.se',
                                 user='lysns',
                                 password=self.pw,
                                 db='lysns',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
        return self
    def __exit__(self, type, value, traceback):
        self.connection.close()

    def get_domain_ids(self):
        with self.connection.cursor() as cursor:
            cursor.execute('SELECT id FROM domain')
            return [item.get("id") for item in cursor.fetchall()]

    def get_domain_name(self, id):
        with self.connection.cursor() as cursor:
            cursor.execute('SELECT name FROM domain WHERE id = %s', id)
            return cursor.fetchone().get("name")

    def get_subdomain_ids(self, domainid):
        with self.connection.cursor() as cursor:
            cursor.execute('SELECT id FROM subdomain WHERE domainid = %s', domainid)
            return [item.get("id") for item in cursor.fetchall()]

    def get_subdomain_name(self, id):
        with self.connection.cursor() as cursor:
            cursor.execute('SELECT name FROM subdomain WHERE id = %s', id)
            return cursor.fetchone().get("name")


    def get_fqdn(self, domain_name, sid):
        subdomain_name = self.get_subdomain_name(sid)
        if subdomain_name.startswith('@'):
            return domain_name
        else:
            return subdomain_name + '.' + domain_name


    def get_key(self, subid):
        with self.connection.cursor() as cursor:
            cursor.execute('SELECT secret FROM update_key WHERE subid = %s LIMIT 1', subid)
            secret = cursor.fetchone()
            if secret is None:
                return None
            else:
                return secret.get("secret")

    def get_record(self, subid):
        with self.connection.cursor() as cursor:
            cursor.execute('SELECT subdomain.name, subdomain.ttl, record_types.name '
                           'FROM subdomain '
                           'INNER JOIN record_types ON subdomain.rr = record_types.id '
                           'WHERE subdomain.id = %s LIMIT 1', subid)
            return cursor.fetchone()

    def get_subdomain_ids_without_keys(self, domainid):
        with self.connection.cursor() as cursor:
            cursor.execute('SELECT subdomain.id '
                           'FROM subdomain '
                           'LEFT OUTER JOIN update_key ON subdomain.id = update_key.subid '
                           'WHERE subdomain.domainid = %s AND update_key.secret IS NULL', domainid)
            return [item.get("id") for item in cursor.fetchall()]

    def set_subdomain_keys(self, domainid, public, private):
        with self.connection.cursor() as cursor:
            res = cursor.execute('INSERT INTO update_key '
                                 '(`subid`, `update_key`, `secret`) VALUES (%s, %s, %s)',
                                 (domainid, public, private,))
            if res == 1:
                self.connection.commit()

            return (res == 1)
