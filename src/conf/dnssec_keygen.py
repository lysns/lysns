import subprocess
import os

class dnssec_keygen():
    def __init__(self):
        pass
    def __enter__(self):
        return self
    def __exit__(self, type, value, traceback):
        pass
    def create_keys(self, fqdn):
        command = '/usr/local/sbin/dnssec-keygen -r /dev/urandom -a HMAC-MD5 -b 512 -K /tmp/ -n HOST'
        args = command.split(' ')
        args += [fqdn]
        with subprocess.Popen(args, stdout=subprocess.PIPE) as proc:
            keyfile = "{}.key".format(proc.stdout.read().decode('utf-8'))
            keyfile = keyfile.replace("\n", "")
            keyfile = "/tmp/" + keyfile
            with open(keyfile, "r") as kf:
                row = kf.readline()
                public_key, part = row.split(" ")[6:8]
                self.public_key = public_key
                self.private_key = (public_key + part).replace('\n', '')
            os.unlink(keyfile)
            privatefile = keyfile.replace("key", "private")
            os.unlink(privatefile)
        return self.public_key, self.private_key
