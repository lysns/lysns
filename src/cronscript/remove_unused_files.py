#!/usr/bin/env python3
import os
from db import DB
path='/usr/local/etc/namedb/master/'
for f in os.listdir(path):
    remove = True
    with DB() as d:
        for id in d.get_domain_ids():
            if f == d.get_domain_name(id):
                remove = False
                break
    if remove:
        print("Remove file {}".format(f))
        # TODO actual file removal
        # TODO logging?
        os.unlink(path + f)
    else:
        print("Keep file {}".format(f))
        # TODO actual file keepal
