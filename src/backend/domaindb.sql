-- TODO why?
delimiter //

DROP TABLE IF EXISTS update_key;//
DROP TABLE IF EXISTS subdomain;//
DROP TABLE IF EXISTS record_types;//
DROP TABLE IF EXISTS domain;//

-- This table is used as an enum table
CREATE TABLE record_types
(
    id int(11) NOT NULL AUTO_INCREMENT,
    name varchar (16) NOT NULL,

    CONSTRAINT pk_record_types_id PRIMARY KEY (id),
    CONSTRAINT uc_record_types_name UNIQUE (name)

)ENGINE=InnoDB;//

CREATE TABLE domain
(
    id int(11) NOT NULL AUTO_INCREMENT,
    name varchar(191), -- hornquist.se
    uid int(11), -- 2278

    CONSTRAINT pk_domain_id PRIMARY KEY (id),
    CONSTRAINT uc_domain_name UNIQUE (name)
)ENGINE=InnoDB;//

CREATE TABLE subdomain
(
    id int(11) NOT NULL AUTO_INCREMENT,
    name varchar(191) NOT NULL, -- blog, `@' for no subdomain
    ttl int(11) DEFAULT 600 NOT NULL,
    class varchar(255) DEFAULT "IN" NOT NULL,
    rr int(11) NOT NULL, -- A, MX
    destination varchar(255), -- only for cname

    domainid int(11),

    CONSTRAINT pk_subdomain_id PRIMARY KEY (id),
    CONSTRAINT fk_subdomain_domid FOREIGN KEY (domainid)
        REFERENCES domain(id)
        ON DELETE CASCADE,
    CONSTRAINT fk_subdomain_rr FOREIGN KEY (rr)
        REFERENCES record_types (id),
    CONSTRAINT uc_subdomain_name_domainid UNIQUE (name, domainid, rr),
    INDEX idx_subdomain_domainid (domainid)

)ENGINE=InnoDB;//

CREATE TABLE update_key
(
    id int(11) NOT NULL AUTO_INCREMENT,
    subid int(11) NOT NULL,

    update_key varchar(255) NOT NULL,
    secret varchar(255) NOT NULL,

    CONSTRAINT pk_update_key_id PRIMARY KEY (id),
    CONSTRAINT fk_update_key_subid FOREIGN KEY (subid)
        REFERENCES subdomain (id)
        ON DELETE CASCADE,
    CONSTRAINT uc_update_key_subid UNIQUE (subid)

)ENGINE=InnoDB;//

-- Hard coded indices to allow this data to be recreated.
INSERT INTO record_types (id, name)
VALUES (1, "A"),
       (2, "AAAA"),
       (3, "CNAME"),
       (4, "TXT"),
       (5, "MX");//
