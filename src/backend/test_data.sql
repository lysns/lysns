DELETE FROM subdomain;
DELETE FROM record_types;
DELETE FROM domain;

INSERT INTO domain (id, uid, name)
VALUES (1, 2278, "hornquist.se"),
       (2, 2278, "hugoweb.ga"),
       (3, 2116, "jiffe.jiff");

INSERT INTO subdomain (id, domainid, name, rr, ttl)
SELECT (1, 1, "@", id, 300) FROM record_types WHERE name = "A";
INSERT INTO subdomain (id, domainid, name, rr, ttl)
SELECT (2, 2, "www", id, 300) FROM record_types WHERE name = "A";
INSERT INTO subdomain (id, domainid, name, rr, ttl)
SELECT (3, 1, "blog", id, 300) FROM record_types WHERE name = "A";
INSERT INTO subdomain (id, domainid, name, rr, ttl)
SELECT (4, 3, "jeff", id, 86400) FROM record_types WHERE name = "AAAA";
