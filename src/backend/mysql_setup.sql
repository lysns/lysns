-- This file is for doing the initial setup of the database
-- It's supposed to be run on mysql.lysator.liu.se
--
-- Replace "'password'" with an actuall password before running.
--
-- DO NOT RUN THIS UNLESS YOU KNOW WHAT YOU ARE DOING
CREATE DATABASE lysns;

CREATE USER 'lysns'@'localhost'
IDENTIFIED BY 'password';
CREATE USER 'lysns'@'%.lysator.liu.se'
IDENTIFIED BY 'password';
CREATE USER 'lysns'@'_gateway'
IDENTIFIED BY 'password';

GRANT ALL PRIVILEGES ON lysns.* TO 'lysns'@'localhost'
    WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON lysns.* TO 'lysns'@'%.lysator.liu.se'
    WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON lysns.* TO 'lysns'@'_gateway'
    WITH GRANT OPTION;
