#!/usr/bin/env python3

# Utility for checking if fully qualified domain names in database can be
# resolved.

from socket import gethostbyname
from dns.resolver import Resolver

from db import DB

def lookup(fully_qualified_domain_name):
    resolver = Resolver()
    resolver.nameservers=[gethostbyname('127.0.0.1')]
    ips = list()
    try:
        ips += [str(x) for x in resolver.query(fully_qualified_domain_name, 'A')]
    except:
        pass
    try:
        ips += [str(x) for x in resolver.query(fully_qualified_domain_name, 'AAAA')]
    except:
        pass
    return ips

with DB() as d:
    for id in d.get_domain_ids():
        dname = d.get_domain_name(id)
        for sid in d.get_subdomain_ids(id):
            sname = d.get_subdomain_name(sid)
            if sname == '*' or sname == '@':
                continue
            fqdn = sname + "." + dname
            print(fqdn)
            print(lookup(fqdn))
